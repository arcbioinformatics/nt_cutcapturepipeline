### Run this script once for every reference fasta you wish to run the Cut Capture pipeline against.
### This will generate the gid bed file with N17s that pipeline will use to check for valid N17s 

if [ "$#" -ne 2 ]; then
    printf "\n \t Run command as: bash getCCbedfiles.sh <fasta_file> <species> \n\n"
    exit 1;
fi


# arg 1 is fasta file
# arg 2 is species prefix

FASTA=$1
SPECIES=$2
#CODEDIR="/tars01/sradhakrishnan/ref/CutCapture/nt_cutcapturepipeline"
CODEDIR=${NT_CC_SCRIPT_DIR}
BASEDIR=`dirname $FASTA`
PREFIX=`basename $FASTA`
TMPFOLDER="/tars01/sradhakrishnan/tmp_sort"
python ${CODEDIR}/getn20_bed.py $FASTA | awk '$4 !~ /N/ && length($4)==34' | gzip > ${BASEDIR}/${PREFIX}_master.bed.gz

zcat ${BASEDIR}/${PREFIX}_master.bed.gz |  
    bioawk -c bed '{if ($6=="-") { 
            print $0, substr(revcomp($4),1,20), substr($4,1,17), substr($4, 18, 17)
        } 
        else {
            print $0, substr($4, 1,20), substr($4, 1,17), substr($4, 18, 17)
        }
    }' | sort -k 7,7 -T ${TMPFOLDER} | gzip > ${BASEDIR}/${PREFIX}_N20_N17_N4GG.bed.gz

LINES=$(zcat ${BASEDIR}/${PREFIX}_N20_N17_N4GG.bed.gz | awk '!a[$7]++' | wc -l)
DIGITS="%0`echo "$LINES" | grep -oE [[:digit:]] | wc -l`g"

paste -d " " <(zcat ${BASEDIR}/${PREFIX}_N20_N17_N4GG.bed.gz | cut -f 7 | sort -T ${TMPFOLDER} | uniq -c | sort -T ${TMPFOLDER} -k 1,1nr) <(seq -f $DIGITS $LINES | sed "s/^/${SPECIES}_/") | sort -T ${TMPFOLDER} -k 2,2 | awk 'BEGIN{OFS="\t";}{print $2, $3"_"$1}' | gzip > ${BASEDIR}/${PREFIX}_gids.gz 


join -1 7 -2 1 -o 1.1,1.2,1.3,1.4,1.5,1.6,0,1.8,1.9,2.2 <(zcat ${BASEDIR}/${PREFIX}_N20_N17_N4GG.bed.gz) <(zcat ${BASEDIR}/${PREFIX}_gids.gz) -t $'\t' | gzip > ${BASEDIR}/${PREFIX}_gids.bed.gz 

bash ${CODEDIR}/getBaselineN17signatures.sh $FASTA ${BASEDIR}/${PREFIX}_gids.bed.gz
