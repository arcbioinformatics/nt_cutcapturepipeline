if [ "$#" -ne 3 ]; then
    printf "\n\t Run command as: bash gg_counts.sh <mapped_read_directory> <gg_position> <filtered/original>\n\n"
    exit 1;
fi

BASEDIR=$1
PREFIX=$(basename $BASEDIR)
DATASET=$(echo $3 | awk '{print tolower($1)}')
OUTDIR="$(dirname $BASEDIR)/CutCapturePipeline/${PREFIX}"
if [ $DATASET == "filtered" ]; then
    BASEDIR="${1}/FILTERED"
fi 
mkdir -p ${OUTDIR}

START=$2
MID=$(expr $START + 1)
END=$(expr $START + 2)

echo "LIBRARY GG_${START} GG_${MID} GG_${END} GG_OVERALL GG_FRAC_${START} GG_FRAC_${MID} GG_FRAC_${END}" > ${OUTDIR}/GGcounts_${PREFIX}.tsv
gg_counts() {
    out_string_gg=""
    out_string_all=""
    start=$2
    end=$(expr $start + 2)
    for i in $(seq $start $end); do
        j=$((i + 1))
        f="${i},${j}"
        samtools view $1 | awk '($6 ~/^[0-9][0-9]M$/){print $10}' | cut -c$f |  sort -T ${TMPFOLDER}| uniq -c | sort -nr > ${1}.${i}.temp
        out_string_gg=${out_string_gg}" "$(grep "GG" ${1}.${i}.temp | awk '{print $1}')
        out_string_all=${out_string_all}" "$(cat ${1}.${i}.temp | awk '{print $1}' | awk '{sum+=$1}END{print sum}')
        rm -f ${1}.${i}.temp
    done
    echo $(basename $1 .mappedonly.bam) $out_string_gg $out_string_all
}

export -f gg_counts
parallel --record-env
parallel -j0 gg_counts ::: `ls ${BASEDIR}/*.mappedonly.bam | sort ` ::: $START | cut -f 1-5 -d " " | sort -k 1,1 | awk 'BEGIN{OFS="\t"}{print $1, $2, $3, $4, $5, $2/$5, $3/$5, $4/$5}' >> ${OUTDIR}/GGcounts_${PREFIX}.tsv

