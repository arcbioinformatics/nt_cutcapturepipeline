You need paired fastq files (files_paired), a design file and a config file to run the pipeline.

also include the the absolute location of your git repo in your bashrc: for example:
export NT_CC_SCRIPT_DIR=/tars01/sradhakrishnan/ref/CutCapture/nt_cutcapturepipeline

Templates for the design and config files are in the git repo. These are tab separated files.

The design file requires the sample names as they're present in the sample sheet (sample name column) for the samples. Column 2 is what guides were used to cut. Column 3 either of 3 options: CUT_i5N7, CUT_N5i7 or UNCUT_i5i7, depending on what channel is being sequenced.

The config file has 4 columns (at this point). The first is the species, second is the name of the bwa map folder, the third is the reference to map and the fourth is just labeled "host" for human, and "nonhost" for anything else. If "nonhost" is present in the config file, the pipeline will generate a list of off-targets in a separate folder. 

IMPORTANT NOTE: YOU NEED TO HAVE ADDITIONAL BED FILES FOR EACH REFERENCE FASTA PRESENT IN THE CONFIG FILE, GENERATED USING THE getCCbedfiles.sh SCRIPT IN THIS FOLDER. THE PIPELINE WILL NOT GENERATE N17 SEQUENCE SIGNATURES OF CUTTING OTHERWISE.  
 
