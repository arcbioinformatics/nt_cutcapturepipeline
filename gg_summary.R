rm(list=ls())
#wd <- "/tars01/sradhakrishnan/data/SequencingRuns/run172/Analysis_CutCapture"
args = commandArgs(trailingOnly=TRUE)
if(length(args)!=1) {
    stop("Need working directory as argument", call.=FALSE)
}
wd <- as.character(args[1])
setwd(wd)
files <- list.files(wd, pattern="GGcounts_.*tsv", recursive=T)

data.df <- do.call(rbind, lapply(files, function(file) {
       d <- read.table(file, header=T)
       d$LIBRARY <- gsub("_S\\d+.*","",d$LIBRARY)
       d$MAP <- basename(gsub(".tsv","",gsub("GGcounts_","",file)))
       d[,c(1,6,7,8,9)]
}))
library(reshape2)
data.df.long <- melt(data.df, id.vars=c("LIBRARY", "MAP"), variable.name="POSITION", value.name="FRACTION")
    gg_starts <- gsub("GG_FRAC_","",data.df.long$POSITION)
    gg_ends <- as.integer(gg_starts)+1
data.df.long$POSITION <- paste("[",gg_starts, ", ", gg_ends, "]", sep="")

n <- length(unique(data.df.long$MAP))
W <- (n+1)*5
H <- nrow(data.df)/n
library(ggplot2)

plot <- ggplot(data.df.long, aes(POSITION, LIBRARY)) +
        geom_tile(aes(fill=FRACTION), colour="white") +
        scale_fill_gradient(low="white", high="red4") + 
        facet_wrap(~MAP) + 
           theme(legend.title=element_text(size=22, face="bold"),
                  legend.text=element_text(size=16, face="bold"), legend.position = "bottom",
              panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
              axis.title.x = element_text(face="bold", size=22),
              axis.title.y = element_text(face="bold", size=22),
              axis.text.x  = element_text(size=22, face="bold"),
              strip.text.x  = element_text(size=24, face="bold"),
              axis.text.y  = element_text(size=18, face="bold"),
              plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm"), 
              legend.key.width=unit(1.5,"in")) +
            labs(x="GG POSITION", y="LIBRARY") 
ggsave(paste(wd, "/CutCapturePipeline/GGcounts_heatmap.pdf", sep=""), plot, width=W, height=H, units="in")

