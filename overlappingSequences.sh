#samtools view CutCapt1_pBRgR_i5N7_N703_i502_S2.lambda.fasta.sorted.mappedonly.bam |  awk '($6 ~/^[0-9][0-9]M$/){print $10}' | cut -c 1-17 | sort | uniq -c | sort -k 2,2 | awk 'BEGIN{OFS="\t";}{print $1, $2}' | join -1 2 -2 8 - <(zcat /tars01/sradhakrishnan/ref/CutCapture/lambda/lambda.fasta_gids.bed.gz) -t $'\t' -o 2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,2.10,1.1,1.2 -a2 -e "0" | wc -l
#5668

#inputs are: 
#$1 = complete directory path of mapped reads
#$2 = GID BED FILE, which is species specific

if [ "$#" -ne 3 ]; then
    printf "\n\t Run command as: bash overlappingSequences.sh <mapped_read_directory> <gid_bed_file> <host/non-host>\n\n"
    exit 1;
fi


export TMPFOLDER="/tars01/sradhakrishnan/tmp_sort"
MAPDIR=$1
GID_BED_FILE=$2
SPECIES_TYPE=$3
BASEDIR=$(dirname $MAPDIR)
PREFIX=$(basename $MAPDIR)
OUTDIR="${BASEDIR}/CutCapturePipeline/${PREFIX}/FILTERED"
mkdir -p $OUTDIR
getOverlaps() {
    bamfile=$1
    base=$(basename $bamfile .mappedonly.bam)
    dirname=$(dirname $bamfile)
    gidbedfile=$2
    outdir=$3
    species_type=$4
    mkdir -p ${outdir}/flagstat
    ln -sf ${dirname}/${base}.bam.flagstat ${outdir}/flagstat/${base}.flagstat 
    #get unique N1-17 bases along with their counts from reads with entire read mapping without softclips, order by sequence; adjust for reverse complements in $10, store final reverse complement of N17.
    if [ ! -f ${outdir}/${base}.revcomp.N17.seqcounts.gz ]; then
    cat <(samtools view -F 16 $bamfile | awk '($6 ~/^[0-9][0-9]M$/){print $10}' | cut -c 1-17) <(samtools view -f 16 $bamfile | awk '($6 ~/^[0-9][0-9]M$/){print $10}' | rev | tr ATGC TACG | cut -c 1-17) | \
       sort -T ${TMPFOLDER} | \
       uniq -c | \
      sort -T ${TMPFOLDER} -k 1,1nr | \
       awk 'BEGIN{OFS="\t";}{print $1, $2}' | bioawk -c bed '{print $1, revcomp($2)}' | gzip > ${outdir}/${base}.revcomp.N17.seqcounts.gz

	#faster awk way relative to join; store N17 counts in hash
#	awk 'BEGIN{OFS="\t";}NR==FNR {hash[$2]=$1; next} ($8 in hash){print $0; hash[$8]}' <(zcat  ${outdir}/${base}.revcomp.N17.seqcounts.gz) <(zcat $gidbedfile) | gzip > ${outdir}/${base}.revcomp_col8.bed.gz
	awk 'BEGIN{OFS="\t";}NR==FNR {hash[$2]=$1; next} ($8 in hash){print $8, hash[$8]}' <(zcat  ${outdir}/${base}.revcomp.N17.seqcounts.gz) <(zcat $gidbedfile) | awk '!a[$0]++' | gzip > ${outdir}/${base}.revcomp_col8.bed.gz
	if [ $species_type != "host" ]; then
		OTDIR="${outdir}/PossibleOffTargets"
		mkdir -p ${OTDIR}

		## The idea here is to calculate the total number of offtarget instances (awk variable sum) per unique N17 (awk variable lines).
		## Column 2, which is the number of times a given N17 is implicated, is expected to be at least thrice as big as the value computed above, in which
		## case it is stored away in an OT.tsv file. The corresponding bed entry is then printed out.
		awk 'BEGIN{OFS="\t";}FNR==NR {sum+=$2; lines+=1; next} {print $0, sum, sum/lines}' <(zcat ${outdir}/${base}.revcomp_col8.bed.gz) <(zcat ${outdir}/${base}.revcomp_col8.bed.gz) | awk '$2 >= 3* $4' > ${OTDIR}/${base}.OT.tsv
		awk 'FNR==NR {hash[$1]=$2" "$3" " $4; next} ($8 in hash){print $0, hash[$8]}'  ${OTDIR}/${base}.OT.tsv <(zcat $gidbedfile) | gzip > ${OTDIR}/${base}.OT.bed.gz 
	fi
    fi
}   
export -f getOverlaps
parallel --record-env
parallel -j10 getOverlaps ::: `ls ${MAPDIR}/FILTERED/*.mappedonly.bam` ::: $GID_BED_FILE ::: $OUTDIR ::: $SPECIES_TYPE

for f in `ls ${OUTDIR}/*revcomp_col8.bed.gz`; do
	b=`basename $f .revcomp_col8.bed.gz`;
	abun=`zcat $f | awk '{sum+=$2}END{print sum}'`
	mapped=`cat ${OUTDIR}/flagstat/${b}.flagstat | head -n 5 | cut -f 1 -d " " | tail -n 1`
	echo $b $abun $mapped
done | awk '{print $0, 100*$2/$3}' > ${OUTDIR}/CutCaptureN17Proportion.tsv


#parallel -j10 getOverlaps ::: `ls ${MAPDIR}/CutCapt1_DIEGOgR_N5i7_i710_N517_S36.GRCh38.fa.sorted.mappedonly.bam` ::: $GID_BED_FILE ::: $OUTDIR
#GID_BED_FILE="/tars01/sradhakrishnan/ref/CutCapture/lambda/lambda.fasta_gids.bed.gz"


##############################

## Get sequence distributions

## sample:
#zcat /tars01/sradhakrishnan/data/SequencingRuns/run172/Analysis_CutCapture/CutCapturePipeline/human_bwamaps/CutCapt1_90kgR_i5N7_N706_i503_S46.GRCh38.fa.sorted_col8.bed.gz | awk '{print $8, $11, $10}' |  awk '!a[$1 $2 $3]++' | sed 's/human.*_//' | head -n 100000 | awk '{arr[$1" "$2]=arr[$1" "$2]+$3}END{for (i in arr){print i, arr[i]}}' | sort -k 2,2nr | less


#getOnTargetGuides() {
#    file=$1
#    outdir=$2
#    dir=$(dirname $file)
#    base=$(basename $file .sorted_col8.bed.gz)
#    revcomp_file=${dir}/${base}.sorted.revcomp_col8.bed.gz
#echo $revcomp_file
#echo $revcomp_file
#    zcat $revcomp_file | bioawk -c bed '{print revcomp($8), $11, $10}' | awk '!a[$1 $2 $3]++' |  sed 's/human.*_//' | awk '{arr[$1" "$2]=arr[$1" "$2]+$3}END{for (i in arr){print i, arr[i]}}' | gzip > ${outdir}/${base}_seqdist.gz
#}
#export -f getOnTargetGuides
#parallel --record-env
#parallel -j10 getOnTargetGuides ::: `ls ${DIR}/CutCapt1_DIEGOgR_N5i7_i710_N517_S36.GRCh38.fa.sorted_col8.bed.gz` ::: $OUTDIR

#if [ $SPECIES_TYPE == "human" ]; then
#    DIST_DIR=${OUTDIR}
#    OUTDIR="${DIST_DIR}/SequenceDistributions"
#    mkdir -p $OUTDIR
#    parallel -j10 getOnTargetGuides ::: `ls ${DIST_DIR}/*.sorted_col8.bed.gz` ::: $OUTDIR
#	 parallel -j10 getOnTargetGuides ::: `ls ${DIST_DIR}/CutCapt1_DIEGOgR_N5i7_i710_N517_S36.GRCh38.fa.sorted_col8.bed.gz` ::: $OUTDIR 
#fi

#echo -ne "LIBRARY\tN17HITS N17HITS_RC\tNOSOFTCLIPS\tTOTAL\tPERC_N17HITS\n" > ${OUTDIR}/CutCaptureSummary.tsv
#for f in $(find $OUTDIR -name "*.N17.seqcounts.gz" | grep -v revcomp); do
#    b=$(basename $f .N17.seqcounts.gz)
#    dir=$(dirname $f)
#    total_mapped=$(cat ${dir}/flagstat/${b}.flagstat | head -n 5 | cut -f 1 -d " " | tail -n 1)
#    total_mapped_nosoftclips=$(zcat $f | awk '{sum+=$1}END{print sum}')

    #n17_hit_count=$(zcat ${dir}/${b}_col8.bed.gz | awk '{sum+=$11}END{print sum}')
    #n17_hit_count_revcomp=$(zcat ${dir}/${b}.revcomp_col8.bed.gz | awk '{sum+=$11}END{print sum}')
    #    hit_count=$(expr $n17_hit_count + $n17_hit_count_revcomp)
    
#    n17_hit_count=$(cat <(zcat ${dir}/${b}.revcomp_col8.bed.gz | bioawk -c bed '{print revcomp($8), $11}') <(zcat ${dir}/${b}_col8.bed.gz | awk '{print $8, $11}') | \
#        awk '!a[$1" "$2]++' | \
#        awk '{sum+=$2}END{print sum}')
#    echo -ne "$b\t$n17_hit_count\t$total_mapped_nosoftclips\t$total_mapped\n"
#done | sort -k 1,1 | awk 'BEGIN{OFS="\t";}{print $1, $2, $3, $4, 100*$2/$3 }' >> ${OUTDIR}/CutCaptureSummary.tsv
