rm(list=ls())
wd <- "/tars01/sradhakrishnan/data/SequencingRuns/run172/Analysis_CutCapture/CutCapturePipeline/human_bwamaps/SequenceDistributions"
setwd(wd)

library(data.table)
library(Biostrings)

files <- list.files(wd, "seqdist.gz")
data <- fread(paste("zcat ", files[1], sep=""), header=F)
colnames(data) <- c("N17", "COUNT", "TOTAL_CUTSITES")
cutoff <- 100
data <- data[data$COUNT>=cutoff]
data$GC <- sapply(data$N17, function(seq) {
    round(sum(alphabetFrequency(DNAString(seq))[c(2,3)])/nchar(seq),3)
})
data$GC_RANGE <- cut(data$GC, breaks=8, include.lowest=T)

data$PROPORTION <- data$COUNT/sum(data$COUNT)

library(ggplot2)
p <- ggplot(data) +
    geom_point(aes(x=PROPORTION, y=TOTAL_CUTSITES, colour=GC_RANGE)) +
    scale_x_log10() +
    scale_y_log10() + 
    facet_wrap(~GC_RANGE)

ggsave("Correlation.pdf", p, width=10, height=10, units="in")
cor(data$PROPORTION, data$TOTAL_CUTSITES)
nrow(data)

library(plyr)
stats <- ddply(data, .(GC_RANGE), summarise, COR=cor(TOTAL_CUTSITES, PROPORTION, method="spearman"), NROW=length(GC_RANGE))
stats
sum(stats$NROW)


