#inputs are: 
#$1 = complete directory path of mapped reads
#$2 = design file

if [ "$#" -ne 2 ]; then
    printf "\n\t Run command as: bash filterBam.sh <mapped_read_directory> <design_file>\n\n"
    exit 1;
fi

export TMPFOLDER="/tars01/sradhakrishnan/tmp_sort"

MAPDIR=$1
DESIGN=$2
OUTDIR="${MAPDIR}/FILTERED/"
mkdir -p $OUTDIR

filter() {
    library=$1
    treatment=$2
    mapdir=$3
    outdir=$4
    bamfile=$(find $mapdir -maxdepth 1 -name "${library}_S*.mappedonly.bam")
    base=$(basename $bamfile .mappedonly.bam)
    flag="-f 64"

    if [ $treatment == "CUT_i5N7" ]; then
        flag="-f 128"
#    elif [ $treatment == "CUT_i5N7" ] ; then
#        flag="-f 128"
    fi 
    echo $library $treatment $bamfile $flag
    if [ ! -f ${outdir}/${base}.mappedonly.bam ]; then
	samtools view -b $flag $bamfile > ${outdir}/${base}.mappedonly.bam
	samtools flagstat ${outdir}/${base}.mappedonly.bam > ${outdir}/${base}.bam.flagstat
    else
	echo " ${outdir}/${base}.mappedonly.bam exists; not regenerating"	
    fi
}
export -f filter
parallel --record-env
parallel --link -j10 filter ::: `cat $DESIGN | awk '{print $1}'` ::: `cat $DESIGN | awk '{print $3}'` ::: $MAPDIR ::: $OUTDIR

