#!/bin/bash

#zcat /tars01/sradhakrishnan/data/SequencingRuns/run172/Analysis_CutCapture/CutCapturePipeline/human_bwamaps/CutCapt1_90kgR_i5N7_N706_i503_S46.GRCh38.fa.sorted_col8.bed.gz | awk '{print $8, $11, $10}' |  awk '!a[$1 $2 $3]++' | sed 's/human.*_//' | head -n 100000 | awk '{arr[$1" "$2]=arr[$1" "$2]+$3}END{for (i in arr){print i, arr[i]}}' | sort -k 2,2nr | less


if [ "$#" -ne 1 ]; then
    printf "\n\t Run command as: bash getOnTargetGuides.sh <directory with col_8 files>\n\n"
    exit 1;
fi


DIR=$1
OUTDIR="${DIR}/SequenceDistributions"
mkdir -p $OUTDIR
getOnTargetGuides() {
    file=$1
    outdir=$2
    dir=$(dirname $file)
    base=$(basename $file .sorted_col8.bed.gz.new)
    revcomp_file="${dir}/${base}.sorted.revcomp_col8.bed.gz.new"
    #    echo $file $revcomp_file
    #combine files, grab the N17, gid and count columns, get unique rows,  and group by sum of cutsites for the N17-and-count combination

#    cat <(zcat ${dir}/CutCapt1_DIEGOgR_N5i7_i710_N517_S36.GRCh38.fa.sorted.revcomp_col8.bed.gz | head -n 500000 | bioawk -c bed '{print revcomp($8), $11, $10}') <(zcat ${dir}/CutCapt1_DIEGOgR_N5i7_i710_N517_S36.GRCh38.fa.sorted_col8.bed.gz | head -n 500000 | awk '{print $8, $11, $10}') | \
    
#    cat <(zcat $file | awk '{print $8, $11, $10}') <(zcat $revcomp_file | bioawk -c bed '{print revcomp($8), $11, $10}') | \  
        
    cat <(zcat $revcomp_file | bioawk -c bed '{print revcomp($8), $11, $10}') <(zcat $file | awk '{print $8, $11, $10}') | \
        awk '!a[$1 $2 $3]++' | \
         sed 's/human.*_//' | \
         awk '{arr[$1" "$2]=arr[$1" "$2]+$3}END{for (i in arr){print i, arr[i]}}' | gzip > ${outdir}/${base}_seqdist.gz
}
export -f getOnTargetGuides
parallel --record-env
#parallel -j10 getOnTargetGuides ::: `ls ${DIR}/CutCapt1_DIEGOgR_N5i7_i710_N517_S36.GRCh38.fa.sorted_col8.bed.gz` ::: $OUTDIR
parallel -j10 getOnTargetGuides ::: `ls ${DIR}/*.sorted_col8.bed.gz.new` ::: $OUTDIR
