if [ "$#" -ne 1 ]; then
	printf "\n\n\tNeed GID bed file as argument\n"
	exit 1;
fi

GID_FILE=$1
DIR=$(dirname $GID_FILE)
BASE=$(basename $GID_FILE .bed.gz)
zcat $GID_FILE | awk 'BEGIN {OFS="\t";}{
	if ($6=="+"){ 
		print $1, $2+16, $2+17, $4, $5, $6, $7, $8, $9, $10
	} else {
		print $1, $2+6, $2+7, $4, $5, $6, $7, $8, $9, $10
	}
}' | gzip > ${DIR}/${BASE}_N17START.bed.gz &

zcat $GID_FILE | awk 'BEGIN {OFS="\t";}{
	if ($6=="+"){ 
		print $1, $2+17, $2+18, $4, $5, $6, $7, $8, $9, $10
	} else {
		print $1, $2+5, $2+6, $4, $5, $6, $7, $8, $9, $10
	}
}' | gzip > ${DIR}/${BASE}_N4GGSTART.bed.gz &
wait
