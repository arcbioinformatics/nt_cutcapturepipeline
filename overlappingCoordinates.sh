
#inputs are: 
#$1 = FILTERED BAM FILE DIRECTORY
#$2 = GID BED FILE, which is species specific
#$3 = DESIGN FILE
#$4 = CONFIG FILE

if [ "$#" -ne 4 ]; then
    printf "\n\t Run command as: bash overlappingCoordinates.sh <filtered_bam_file_dir> <gid_bed_file> <design_file> <config_file>\n\n"
    exit 1;
fi


export TMPFOLDER="/tars01/sradhakrishnan/tmp_sort"
FILTERED_BAM_DIR=$1
GID_BED=$2

#FILTERED_BAM_DIR="/tars01/sradhakrishnan/data/SequencingRuns/run172/Analysis_CutCapture/lambda_bwamaps_2/FILTERED"
#CutCapt1_DIEGOgR_i5N7_N705_i504_S48.lambda.fasta.sorted.mappedonly.bam"
#GID_BED="/tars01/sradhakrishnan/ref/CutCapture/lambda/lambda.fasta_gids.bed.gz"

GID_PRE=${GID_BED/gids.bed.gz/gids_}

OUT_PRE=$(dirname $(dirname $FILTERED_BAM_DIR))
BASE=$(basename $(dirname $FILTERED_BAM_DIR))
OUTDIR="${OUT_PRE}/CutCapturePipeline/${BASE}/FILTERED_COORDS"

echo "Making directory $OUTDIR to write out overlapping coord results"
mkdir -p $OUTDIR
getOverlaps() {
	bamfile=$1
	gid_bed=$2
	outdir=$3
	prefix=$(basename $bamfile .mappedonly.bam)
	if [[ $gid_bed =~ N17 ]]; then
		bed_pre="N17"
	else
		bed_pre="N4GG"
	fi
	echo "BAM is $bamfile"
	echo "GID is $gid_bed"
	echo "OUTFOLDER is $outdir"
	samtools view -h $bamfile | awk '$1 ~ /^@/ || $6 ~ /^[0-9]{2}M$/' | samtools view -hbS - | bedtools bamtobed -ed -i - | awk 'BEGIN{OFS="\t";} FNR==NR {
			hash[$1"_"$2"_"$3]; next
		} {
			end=$2+1; 
			start=$3-1; 
			if ($1"_"$2"_"end in hash && !($1"_"start"_"$3 in hash)) {
				print $0, 0, 1
			} else 
			if (!($1"_"$2"_"end in hash) && $1"_"start"_"$3 in hash) {
				print $0, 1, 0
			} else
			if ($1"_"$2"_"end in hash && $1"_"start"_"$3 in hash) {
				print $0, 1, 1
			} else {
				print $0, 0, 0
			}
		}' <(zcat $gid_bed) - | gzip > ${outdir}/${prefix}_${bed_pre}STARTS.bed.gz
	 
}
export -f getOverlaps
parallel --record-env
parallel -j6 getOverlaps ::: $(ls $FILTERED_BAM_DIR/*.mappedonly.bam) ::: `ls $GID_PRE*` ::: $OUTDIR

DESIGN_FILE=$3
CONFIG_FILE=$4

OUTDIR_SUMMARY="${OUT_PRE}/CutCapturePipeline/${BASE}/FILTERED_COORDS_SUMMARY"
mkdir -p $OUTDIR_SUMMARY

summarize() {
	library=$1
	dir=$2
	outdir=$3
	printf "N17_L\tN17_R\tN4GG_L\tN4GG_R\tTOTAL_COUNT\n" > ${outdir}/${library}_N17N4GGsummary.tsv
	paste -d "\t" <(zcat ${dir}/${library}*N17STARTS.bed.gz | cut -f 7,8) <(zcat ${dir}/${library}*N4GGSTARTS.bed.gz | cut -f 7,8) | awk 'BEGIN{OFS="\t";}{arr[$0]++}END{for (i in arr){print i, arr[i];}}' | sort -k 5,5nr >> ${outdir}/${library}_N17N4GGsummary.tsv
}
export -f summarize
parallel --record-env
parallel -j0 summarize ::: $(cat $DESIGN_FILE | awk '{print $1}') ::: $OUTDIR ::: $OUTDIR_SUMMARY

SPECIES=$(cat $CONFIG_FILE | awk -v folder=$BASE '($2==folder){print $1}')
Rscript ${NT_CC_SCRIPT_DIR}/overlappingCoordinates.R $OUTDIR_SUMMARY $DESIGN_FILE $SPECIES
