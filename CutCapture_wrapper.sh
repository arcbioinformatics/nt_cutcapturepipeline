if [ "$#" -ne 3 ]; then
	printf "\n\n\tNeed files_paired, design and config in that order\n\n"
	exit 1;
fi

if [ ${NT_CC_SCRIPT_DIR} == "" ]; then
	printf "\nenv variable NT_CC_SCRIPT_DIR not set - set this to the CutCapturePipeline git installation directory and try again\n"
	exit 1;
fi

#### Inputs ####
## $1: files_paired
## $2: design
## $3: config

FILES_PAIRED=$1
DESIGN=$2
CONFIG=$3
export CWD=$(pwd -P)
export TMPFOLDER="/tars01/sradhakrishnan/tmp_sort"
mkdir -p "${CWD}/logs"

printf "\n\nKicking off pipeline. Check ${CWD}/logs for module-specific logs"
#### Step 1: fastqc + multiqc ####
#### Needs: R1 and R2 files ####
fastqc_method() {
	file_pre=`basename $1 _001.fastq.gz`
	if [ ! -d ${CWD}/fastqc/${file_pre} ]; then
		mkdir -p "${CWD}/fastqc/${file_pre}"
		mkdir -p "${CWD}/logs/fastqc"
		echo "Running fastqc for ${file_pre}"
		fastqc -o "${CWD}/fastqc/${file_pre}" -t 6 $1 > ${CWD}/logs/fastqc/${file_pre}.FASTQC.OUT 2> ${CWD}/logs/fastqc/${file_pre}.FASTQC.ERR
	else
		echo "${CWD}/fastqc/${file_pre} exists; skipping fastqc"
	fi
}

export -f fastqc_method
parallel --record-env

parallel -j0 fastqc_method ::: $(cat $FILES_PAIRED | sed 's/ /\n/')
if [ ! -f "${CWD}/multiqc_report.html" ]; then
	echo "Generating MultiQC report"
	multiqc -f .
else
	echo "MultiQC Report exists, not regenerating."
fi

printf "\n\nStep 1: Fastqc done. MultiQC report generated. Proceeding to BWA mapping...\n\n"
#### Step 2: bwa and friends; map to references described in config ####
#### Needs: R1 and R2 from files_paired, references and output folders from config ####

bwa_method() {
	files_paired=$1
	reference=$2
	outfolder=$3
	mkdir -p "${CWD}/logs/bwa"
	bwa_script="/mnt/bdbStorage01/sradhakrishnan/pipelines/bwa/bwa_generic.sh"
	if [[ ! -d $outfolder ]]; then
		bash $bwa_script $files_paired $reference $outfolder > ${CWD}/logs/bwa/${outfolder}.BWA.OUT 2> ${CWD}/logs/bwa/${outfolder}.BWA.ERR
	else
		echo "$outfolder exists; skipping BWA step"
	fi	
}
export -f bwa_method
parallel --record-env
parallel --link -j2 bwa_method ::: $FILES_PAIRED ::: $(cat $CONFIG | awk '{print $3}') ::: $(cat $CONFIG | awk '{print $2}')
printf "\n\nStep 2: BWA mappings complete. Proceeding to filter reads...\n\n"

#### Step 3: filterBam.sh to remove R1 or R2 depending on i5N7/N5i7 as described in the design ####
#### Needs: Mapped read directory per species and design file ####

filterBam() {
	cwd=$1
	mapfolder=$2
	design=$3
	mkdir -p "${CWD}/logs/filterBam"
	filterBam_script="${NT_CC_SCRIPT_DIR}/filterBam.sh"
	bash $filterBam_script ${cwd}/${mapfolder} $design > ${CWD}/logs/filterBam/${mapfolder}.FILTERBAM.OUT 2> ${CWD}/logs/filterBam/${mapfolder}.FILTERBAM.ERR
}
export -f filterBam
parallel --record-env
parallel -j1 filterBam ::: ${CWD} ::: $(cat $CONFIG | awk '{print $2}') ::: $DESIGN
printf "\n\nStep 3: Bam file filtering complete. Moving on to GG distribution calculation and plots\n\n"

#### Step 4: GG counts (bash) + heatmap (R) to look at GG distribution in the libraries across species mentioned in config ####
#### Needs: Mapped read directory per species and design file ####
#### Note: Running this with the filtered dataset, see $3 in gg_counts function

GG_START_POSITION=5
DATASET="filtered"

gg_counts() {
	mapfolder=$1
	startpos=$2
	dataset=$3
	mkdir -p "${CWD}/logs/gg_counts"
	gg_counts_script="${NT_CC_SCRIPT_DIR}/gg_counts.sh"
	bash $gg_counts_script $mapfolder $startpos $dataset > ${CWD}/logs/gg_counts/${mapfolder}.GG_COUNTS.OUT 2> ${CWD}/logs/gg_counts/${mapfolder}.GG_COUNTS.ERR
	
}
export -f gg_counts
parallel --record-env
parallel -j0 gg_counts ::: $(cat $CONFIG | awk '{print $2}') ::: $GG_START_POSITION ::: $DATASET 
GG_SUMMARY_SCRIPT="${NT_CC_SCRIPT_DIR}/gg_summary.R"
Rscript $GG_SUMMARY_SCRIPT $CWD

printf "\nGenerated GG frequency plots \n\nStarting n17 analysis:"

#### Step 5: N17 counts (bash) + scatter plots (R) to look at % reads showing N17 signatures across species mentioned in config and as described in design ####
#### This module also prints out putative off-target regions in non-human species where the signature is higher than expected at random
#### Needs: Mapped read directory per species and corresponding gid bed file ####

n17() {
	mapfolder="${CWD}/$1"
	gidfile=$2
	host_type=$3
	n17_script="${NT_CC_SCRIPT_DIR}/overlappingSequences.sh"
	mkdir -p "${CWD}/logs/n17"
	echo "Running $n17_script on $mapfolder with $gidfile as $host_type species"
	bash $n17_script $mapfolder $gidfile $host_type > ${CWD}/logs/n17/${1}.N17.OUT 2> ${CWD}/logs/n17/${1}.N17.ERR
}
export -f n17
parallel --record-env
parallel --link -j10 n17 ::: $(cat $CONFIG | awk '{print $2}') ::: $(cat $CONFIG | awk '{print $3"_gids.bed.gz"}') ::: $(cat $CONFIG | awk '{print $4}')

printf "\n\nGenerated N17 numbers\n\nMoving on to plotting..."
N17_RSCRIPT="${NT_CC_SCRIPT_DIR}/N17_plots.R" 
Rscript $N17_RSCRIPT $CWD $DESIGN $CONFIG
printf "...done.\n\nEnd of pipeline\n\n"

#### Clean up ####
if [ -d ${CWD}/CutCapturePipeline/logs ]; then
	rm -rf ${CWD}/CutCapturePipeline/logs
fi
mv -f ${CWD}/logs ${CWD}/CutCapturePipeline/
