if [ "$#" -ne 2 ]; then
    printf "\n\t Run command as: bash getBaselineN17signatures.sh <fasta_file> <gid_bed_file.gz>\n\n"
    exit 1;
fi
FASTA=$1
GIDBED=$2
BASE=$(basename $FASTA)
DIR=$(dirname $FASTA)
bioawk -c fastx '{print $name, length($seq)}' $FASTA > ${DIR}/${BASE}.genome

NUMBER_GUIDES=$(zcat $GIDBED | wc -l)
TOTAL_GENOME_LENGTH=$(cat ${DIR}/${BASE}.genome | awk '{sum+=$2}END{print sum}')

echo -ne "NUMBER_GUIDES\tTOTAL_GENOME_LENGTH\n" > ${DIR}/${BASE}.stats
echo -ne "$NUMBER_GUIDES\t$TOTAL_GENOME_LENGTH\n" >> ${DIR}/${BASE}.stats
